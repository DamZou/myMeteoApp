myMeteoApp.controller('meteoByCityController', function($scope, $http){
    $scope.getWeather=function(){
        var city = $scope.city;
        if (!city){
            $scope.meteo = false;
            $scope.pasmeteo = true;
            $scope.resultat = "Vous n'avez pas saisi de ville";
        } else {
            $http({
              method: 'GET',  
              url: 'http://api.openweathermap.org/data/2.5/weather?q='+city+'&appid=0b2a00bcf3dcf14c6ed23b1ed76cfa86&lang=fr'
            }).then(function(response) {
                if (response.data.cod === '404'){
                    $scope.main = 'Ville est introuvable';
                    return;
                }
                $scope.meteo = true;
                $scope.pasmeteo = false;
                $scope.station = response.data.name;
                $scope.main = response.data.weather[0].main;
                $scope.description = response.data.weather[0].description;
                $scope.sunset = new Date(response.data.sys.sunset*1000);
                $scope.sunrise = new Date(response.data.sys.sunrise*1000);
                $scope.temp = response.data.main.temp;
                $scope.humidity = response.data.main.humidity;
              })
        }
    };
});