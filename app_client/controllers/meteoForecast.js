myMeteoApp.controller('meteoForecastController', function($scope, $http, $routeParams, $rootScope, tweetWidgets){
    tweetWidgets.destroyAllWidgets();
    tweetWidgets.loadAllWidgets();
    var city = $routeParams.ville
    $http({
    method: 'GET',  
    url: 'http://api.openweathermap.org/data/2.5/forecast/daily?q='+city+'&appid=0b2a00bcf3dcf14c6ed23b1ed76cfa86&lang=fr'
        }).then(function(response) {
            if (response.data.cod === '404'){
                $scope.main = 'Ville est introuvable';
                return;
        }
        $scope.city = response.data.city.name
        $scope.meteos = response.data.list
    })
});

myMeteoApp.service('tweetWidgets', function() {
        this.loadAllWidgets = function() {
            !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
         };

         this.destroyAllWidgets = function() {
            var $ = function (id) { return document.getElementById(id); };
            var twitter = $('twitter-wjs');
            if (twitter != null)
                twitter.remove();
    };  
});