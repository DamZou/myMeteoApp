myMeteoApp.controller('meteoSelfController', function($scope, $http, $rootScope){
    getMeteo();
    setInterval(function(){getMeteo()}, 600000);
    function getMeteo(){
        navigator.geolocation.getCurrentPosition(success, error);
    }
    function success(position) {
            $http({
            method: 'GET',  
            url: 'http://api.openweathermap.org/data/2.5/weather?lat=' + position.coords.latitude + '&lon=' + position.coords.longitude + '&appid=0b2a00bcf3dcf14c6ed23b1ed76cfa86&lang=fr'
            }).then(function(response) {
                $scope.affichage = true;
                $scope.station = response.data.name;
                $scope.main = response.data.weather[0].main;
                $scope.description = response.data.weather[0].description;
                $scope.sunset = new Date(response.data.sys.sunset*1000);
                $scope.sunrise = new Date(response.data.sys.sunrise*1000);
                $scope.temp = response.data.main.temp;
                $scope.humidity = response.data.main.humidity;
            })
    }
    function error(err) {
        $rootScope.withoutGeo = true;
        console.log(err)
    }
});