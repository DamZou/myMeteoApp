var myMeteoApp = angular.module('myMeteoApp',['ngRoute','ui.bootstrap','ngAnimate']);

myMeteoApp.config(['$routeProvider', '$locationProvider',
	function($routeProvider, $locationProvider){
	    $routeProvider
	    .when('/', {
	    templateUrl : 'app_client/views/france.html',
	    controller : 'meteoFranceController'
	    })
	    .when('/ville', {
	    templateUrl : 'app_client/views/searchByCity.html',
	    controller : 'meteoByCityController'
	    })
	    .when('/:ville', {
		templateUrl : function(city){
			return 'app_client/views/cities/' + city.ville + '.html';
		},
		controller : 'meteoForecastController'
	    })
	   	.otherwise({
		template: "does not exists"
	    });
	    $locationProvider.html5Mode(true)
}]);